package com.kwil3092_tpur9893.assignment1.algorithms;

import java.util.ArrayList;
import java.util.List;

/**
 * The KNN class calculates the returns the accuracy of the k
 * Nearest neighbour classifier
 * @author Tomislav, Kerrod
 *
 */
public class KNN {
	private int K;
	private List<Fold> trainingEntries = new ArrayList<Fold>();
	public List<Entries> distances = new ArrayList<Entries>();
	private int accuracy = 0;
	private float sum = 0, totalAccuracy = 0;;
	
	/**
	 * 
	 * @param trainingData
	 * @param testingEntry
	 */
	
	public void init(int K) {
		this.K = K;
	}
	
	/**
	 * Trains the data of the given testing entry Fold against the list of training entry Folds
	 * @param trainingEntries is the list of training Folds for training
	 * @param testingEntry is the Fold to test
	 * @return the accuracy of the KNN of the testing Entry against the training Entries
	 */
	public float trainData(List<Fold> trainingEntries, Fold testingEntry) {
		this.trainingEntries = trainingEntries;
		
		// Calculates euclidean distance for all training entries against the testing entry
		for (int i = 0; i < trainingEntries.size(); i++) {
			// Test testing fold against training folds
			List<Entries> testData = testingEntry.entries;
			for (int j = 0; j < testData.size(); j++) {
				// Create table of distances of all training entries for the testing entry
				calcEuclideanDistances(testData.get(j), trainingEntries.get(i));
				// if the knn result of the testing entry was correct, increment the accuracy
				if (testData.get(j).classVariable.endsWith(getResult(distances))) accuracy++;
												
			}
			// sum all the accuracies of the results
			sum += ((float)accuracy/trainingEntries.get(i).entries.size() * 100);
			accuracy = 0;
			
		}
		totalAccuracy = (sum/trainingEntries.size());
		sum = 0;
		return totalAccuracy;
	}
	
	/**
	 * Determines the class of the testing entry by applying the KNN algorithms
	 * @return the class of the testing entry
	 */
	public String getResult(List<Entries> distances) {
		if (trainingEntries.isEmpty()) {
			System.out.println("No training data found. Cannot determine node");
			return null;
		}
		
		int majority = determineMajority(getNearestNeighbours (distances));
		
		return "class" + majority;
	}
	
	/**
	 * Calculates the Euclidean Distance of each Entry in the training data from the test data
	 * @param e is the Entry which we are using for the test data
	 * pre- condition is that an Entry from a list of Entries in 'entries' is passed
	 * post- condition is that the distance between these values will be calculated and updated
	 * accordingly specific to the test data given
	 */
	
	private void calcEuclideanDistances(Entries e, Fold trainingEntry) {
		//List<Entries> distances = new ArrayList<Entries>();
		distances.clear();
		distances.addAll(trainingEntry.entries);
		
		// RESET ALL DISTANCES
		e.euclideanDistance = 0;
		for (Entries entry : distances) {
			entry.euclideanDistance = 0;
		}
		
		// Sets the distance value of all training elements to the corresponding testing entry
		for (int i = 0; i < e.data.length; i++) {	
			for (Entries entry : distances) {
				entry.distance[i] = (float) Math.pow((e.data[i] - entry.data[i]), 2);
			}
		}
		
		// Computes the euclidean distance of each training entry to the testing entry
		for (Entries a : distances) {
			for (int i = 0; i < e.data.length; i++) {
				a.euclideanDistance += a.distance[i];
			}
			a.euclideanDistance = (float) Math.sqrt(a.euclideanDistance);
		}
	} 
	
	
	/**
	 * Finds the K Nearest Neighbours of the training data based on their Euclidean
	 * Distance from the current Entry being tested
	 * @param distances, contains all the Entries with the updated values
	 * @return returns the K Nearest Neighbours
	 */
	
	public List<Entries> getNearestNeighbours(List<Entries> distances) {
		List<Entries> nearestNeighbours = new ArrayList<Entries>();
		for (int i = 0; i < K; i++) {
			Entries closest = null;
			closest = getMin(distances);
			nearestNeighbours.add(closest);
			distances.remove(closest);
		}
		return nearestNeighbours;
	}
	
	/**
	 * Gets the entry with the smallest distance to the testing entry
	 * @param distances is the list of entries in the training data
	 * @return the entry with the smallest distance to the testing entry
	 */
	private Entries getMin(List<Entries> distances) {
		Entries min = distances.get(0);
		for (Entries e : distances) {
			if (e.euclideanDistance < min.euclideanDistance)
				min = e;
		}
		return min;
	}
	
	/**
	 * Determines the majority of class 1 and class 0 entries in the given list.
	 * This list is the size of K and is usually an odd number so it can easily be
	 * determined as to which class has more than the other
	 * @param neighbours the K number of entries closest to the testing entry
	 * @return the class which has the majority
	 */
	public int determineMajority(List<Entries> neighbours) {
		int yes = 0;
		
		for (int i = 0; i < neighbours.size(); i++) {
			if (neighbours.get(i).classVariable.endsWith("1")) yes++;
			
		}
		return yes > neighbours.size()/2 ? 1 : 0;
	}
	
	/**
	 * Sets the radius of the Nearest Neighbour to K
	 * @param K, integer variable to represent K nearest nodes
	 * pre- condition is that K should be an odd number
	 * post- condition is that K will always be odd
	 */
	public void setK(int K) {
		if (K%2 == 0) {
			System.out.println("K must be an odd number. Setting K to default value of 1");
			this.K = 1;
		}
		else
			this.K = K;
	}
}
