package com.kwil3092_tpur9893.assignment1.algorithms;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.kwil3092_tpur9893.assignment1.display.DataFrame;

/**
 * The FileReader class handles with the reading and writing of the .csv
 * files for the application
 * @author Tomislav, Kerrod
 *
 */
public class FileReader {

	public List<Entries> trainingData = new ArrayList<Entries>();
	public String[] dataTypes;
	
	/**
	 * Prints all the entries into a .csv file and saves the file in the local relative directory
	 * @param file is the name of the saved file
	 * @throws FileNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	public void write(String file) throws FileNotFoundException, UnsupportedEncodingException {
		PrintWriter writer = new PrintWriter(file, "UTF-8");
		for (int i = 0; i < DataFrame.folds.length; i++) {
			writer.println("fold" + (i + 1));
			Fold f = DataFrame.folds[i];
			for (int j = 0; j < f.size(); j++) {
				writer.println(f.entries.get(j).print());
			}
			writer.println();
		}
		
		writer.close();
		System.out.println("New File has been made");
	}
	
	/**
	 * Reads all the entries from a .csv file and splits the data elements into a list of Strings
	 * @param file is the name of the file to load
	 * @throws IOException
	 * @throws FileNotFoundException
	 * @throws NullPointerException
	 */
	public void read(String file) throws IOException, FileNotFoundException, NullPointerException {

		List<Entries> trainingData = new ArrayList<Entries>();

		InputStream is = this.getClass().getResourceAsStream(file);
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		
		String strLine = br.readLine();
		dataTypes = strLine.split(",");
		while ((strLine = br.readLine()) != null) {
			List<String> items = Arrays.asList(strLine.split(","));
			Entries newEntry = new Entries(items);
			trainingData.add(newEntry);
		}
		br.close();
		this.trainingData.clear();
		this.trainingData.addAll(trainingData);
	}
	
}
