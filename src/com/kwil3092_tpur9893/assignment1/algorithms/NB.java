package com.kwil3092_tpur9893.assignment1.algorithms;

import java.util.ArrayList;
import java.util.List;

/**
 * The NB class calculates the returns the accuracy of the
 * Naive Bayes classifier
 * @author Tomislav, Kerrod
 *
 */
public class NB {

	List<Entries> test = new ArrayList<Entries>();
	List<Entries> class0 = new ArrayList<Entries>();
	List<Entries> class1 = new ArrayList<Entries>();
	List<Float> mean0 = new ArrayList<Float>();
	List<Float> mean1 = new ArrayList<Float>();
	List<Float> sd0 = new ArrayList<Float>();
	List<Float> sd1 = new ArrayList<Float>();
	int numOfAtt;
	float probzero;
	float probone;
	float totalAccuracy;

	/**
	 * Trains the data of the given testing entry Fold against the list of training entry Folds
	 * @param trainingEntries is the list of training Folds for training
	 * @param testingEntry is the Fold to test
	 * @return the accuracy of the NB of the testing Entry against the training Entries
	 */
	public float trainData(List<Fold> trainingEntries, Fold testingEntry) {

	
		// Since trainData is called once, and is needed for each step, we refresh all variables.
		totalAccuracy = 0;
		numOfAtt = trainingEntries.get(0).entries.get(0).size;
		
		// Split TD into case0 and case1
		for (Fold f : trainingEntries) {
			class0.clear();
			class1.clear();
			List<Entries> ent = f.entries;
			for (Entries e : ent) {
				if (e.classVariable.equals("class0")) {
					class0.add(e);
				} else if (e.classVariable.equals("class1")) {
					class1.add(e);
				} else {
					System.out.println("An invalid Case exists");
				}
			}
			//System.out.print("Classes have been sorted, ");
			int wholeSize = class0.size()+class1.size();
			this.probzero = (((float)class0.size()) / (float)wholeSize);
			this.probone = (((float)class1.size()) / (float)wholeSize);
			// Find Mean and SD for each attribute.
			this.mean0.clear();
			this.mean0 = findMean(class0);
			this.mean1.clear();
			this.mean1 = findMean(class1);
			this.sd0.clear();
			this.sd0 = findSD(mean0, class0);
			this.sd1.clear();
			this.sd1 = findSD(mean1, class1);
			
			test.clear();
			List<Entries> test = testingEntry.entries;
			
			float runAcc = 0;
			runAcc = testData(test);
			totalAccuracy += runAcc;
		}
		;
		int numFolds = trainingEntries.size();

		totalAccuracy = totalAccuracy/numFolds;
		
		return totalAccuracy;
	}

	/**
	 * 
	 * @param test
	 * @return
	 */
	public float testData(List<Entries> test) {
		int correct = 0;
		int wrong = 0;
		
		for (Entries e : test){
			ArrayList<Float> prob0 = new ArrayList<Float>();
			ArrayList<Float> prob1 = new ArrayList<Float>();
			for (int i = 0; i < (numOfAtt-1); i++)
			{
				float input = e.get(i);
				// Calc. for prob0 
				float sd = this.sd0.get(i);
				float mean = this.mean0.get(i);
				float p = (float) ((1 / (sd * Math.sqrt(2 * Math.PI))) * Math.exp((-1) * ( ((input - mean)*(input - mean))/(2 * (sd*sd)))));
				prob0.add(p);
				// Calc. for prob1 
				sd = this.sd1.get(i);
				mean = this.mean1.get(i);
				p = (float) ((1 / (sd * Math.sqrt(2 * Math.PI))) * Math.exp((-1) * ( ((input - mean)*(input - mean))/(2 * (sd*sd)))));
				prob1.add(p);				
			}
			float probzero = prob0.get(0);
			float probone = prob1.get(0);
			
			for (int i = 1; i < (numOfAtt-1); i++){
				probzero *= prob0.get(i);
				probone *= prob1.get(i);
			}

			// Predict it a class
			String classV = null;
			if (probzero > probone){
				classV = "class0";
			} else if (probzero < probone){
				classV = "class1";
			} else {
				System.out.println("There is an equal chance");
			}
				
			// Check to see if class is correct.
			if (e.classVariable.equals(classV)){
				correct++;
			} else {
				wrong++;
			}
			
			
		}
		
		float accuracy = ((((float)correct)/ ((float)(correct+wrong))) * 100.00f);
		
		return accuracy;
		
	}
	
	/**
	 * Calculates the mean of the list of entries for each data element in 
	 * all the entries
	 * @param L is the list of entries
	 * @return the mean for all the data elements
	 */
	public List<Float> findMean(List<Entries> L) {
		List<Float> mean = new ArrayList<Float>();

		int size = L.size();
		int attSize = L.get(0).size;
		float[] runTotals = new float[attSize-1];

		for (Entries e : L){
			int i = 0;
			for (int j = 0; j < attSize-1; j++){
				float f = e.data[j];
				runTotals[i] += f;
				i++;
			}
			
		}
		for (float f : runTotals){
			float newMean = f / size;
			mean.add(newMean);
		}

		return mean;
	}

	/**
	 * Calculates the standard deviation of the list of entries for each
	 * data element in all the entries
	 * @param mean of the data elements in all the entries
	 * @param L is the list of entries
	 * @return the standard deviation for all the data elements
	 */
	public List<Float> findSD(List<Float> mean, List<Entries> L) {
		List<Float> sd = new ArrayList<Float>();

		float var;

		for (int i = 0; i < (this.numOfAtt-1); i++) {
			var = 0;

			for (Entries e : L) {
				var += (mean.get(i) - e.get(i)) * (mean.get(i) - e.get(i));
			}
			var = var / L.size();
			sd.add((float) Math.sqrt(var));
		}

		return sd;
	}

}

	
	

	
	

