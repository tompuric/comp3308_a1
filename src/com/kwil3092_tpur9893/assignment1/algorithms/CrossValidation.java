package com.kwil3092_tpur9893.assignment1.algorithms;

import java.util.List;
import java.util.ArrayList;

import com.kwil3092_tpur9893.assignment1.display.DisplayResults;

/**
 * The CrossValidation class takes in an array of stratified folds which were predetermined
 * before in the DataFrame class. It then runs the cross validation algorithm against
 * the stratified folds by running the KNN or NB algorithms with 1 fold of testing data and
 * 9 folds of training data which it then iterates through all the folds taking turns for being
 * the testing data and prints the average result at the end.
 * @author Tomislav, Kerrod
 *
 */
public class CrossValidation {
	
	public static final int K_FOLDS = 10;
	
	private Fold[] folds = new Fold[K_FOLDS];
	
	public KNN knn = new KNN();
	public NB nb = new NB();
	
	public float knnAccuracy;
	public float nbAccuracy;
	
	private List<Fold> trainingData = new ArrayList<Fold>();
	private Fold testingData = new Fold();
	
	/**
	 * Initialises the folds for the cross validation. These folds were
	 * determined earlier in the DataFrame class
	 * @param folds of all the data
	 */
	public void init(Fold[] folds) {
		this.folds = folds;
	}
	
	/**
	 * Performs Cross Validation using the kNN classifier. It iterates through K_FOLDS
	 * of folds, testing the Kth Fold against the other training Folds and prints the
	 * accuracy of each testing Fold.
	 * @param k for k Nearest Neighbours
	 */
	public void tickKNN(int k) {
		knn.init(k);
		DisplayResults.setKNNText("");
		
		float accuracy = 0;
		
		// For K_FOLDS, do
		for (int j = 0; j < K_FOLDS; j++) {
			trainingData.clear();
			// Set the Kth fold as the testing data
			testingData = folds[j];
			// Add all the other K_FOLDS to the training data
			for (int i = 0; i < folds.length; i++) {
				if (i == j) continue;
				trainingData.add(folds[i]);
			}
			// Display the result gathered from kNN
			float val = knn.trainData(trainingData, testingData);
			DisplayResults.appendKNNText("KNN accuracy for fold "+j+": " + val + "\n");
			accuracy += val;
		}
		// Display the average KNN of all the results from each fold that was tested
		knnAccuracy = accuracy/K_FOLDS;
		DisplayResults.appendKNNText("Total KNN accuracy is: " + knnAccuracy + "\n");
	}
	
	/**
	 * Performs Cross Validation using the NB classifier. It iterates through K_FOLDS
	 * of folds, testing the Kth Fold against the other training Folds and prints the
	 * accuracy of each testing Fold.
	 */
	public void tickNB() {
		DisplayResults.setNBText("");
		float accuracy = 0;
		// For K_FOLDS do
		for (int j = 0; j < K_FOLDS; j++) {
			trainingData.clear();
			// Set the Kth fold as the testing data
			testingData = folds[j];
			// Add all the other K_FOLDS to the training data
			for (int i = 0; i < folds.length; i++) {
				if (i == j) continue;
				trainingData.add(folds[i]);
			}
			// Display the result gathered from NB
			float val = nb.trainData(trainingData, testingData);
			DisplayResults.appendNBText("NB accuracy for fold "+j+": " + val + "\n");
			accuracy += val;
		}
		// Display the average NB of all the results from each fold that was tested
		nbAccuracy = accuracy/K_FOLDS;
		DisplayResults.appendNBText("Total NB accuracy is: " + nbAccuracy + "\n");
	}
	
}
