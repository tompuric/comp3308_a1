package com.kwil3092_tpur9893.assignment1.algorithms;

import java.util.List;
import java.util.ArrayList;

/**
 * The Fold class deals with storing a list of entries that compromises
 * one Fold of entries. A fold of entries is at a size of totalEntries/K_FOLDS
 * @author Tomislav, Kerrod
 *
 */
public class Fold {
	
	public List<Entries> entries = new ArrayList<Entries>();

	/**
	 * Adds an Entry to the Fold
	 * @param e is the Entry to add
	 */
	public void add(Entries e) {
		entries.add(e);
	}

	/**
	 * returns the amount of Class 1 and Class 0 entries in the Fold
	 * @return a String of the sum of Class 1 and Class 0 entries in the Fold
	 */
	public String print() {
		int good = 0, bad = 0;
		for (Entries e : entries) {
			if (e.classVariable.endsWith("1")) good++;
			else bad++;
		}
		return " - " + good + " class 1 entries.\n" + 
		       " - " +  bad + " class 0 entries.\n";
	}
	
	/**
	 * Prints the list of entries in the fold
	 * @return a String containing the list of entries in the fold in a String format
	 */
	public String printData() {
		String str = "";
		for (Entries e : entries) {
			str += e.toString() + "\n";
		}
		return str;
	}
	
	/**
	 * @return the size of the Fold. The amount of entries in the fold
	 */
	public int size() {
		return entries.size();
	}

}
