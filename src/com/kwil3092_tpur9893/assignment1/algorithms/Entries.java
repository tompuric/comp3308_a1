package com.kwil3092_tpur9893.assignment1.algorithms;

import java.util.*;

/**
 * The Entries class contains the list of data elements in an entry
 * in the data provided along with its classification. It also provides
 * the data for the distance between the entry and its testing entry for
 * the KNN algorithm. Along with its euclidean distance. Lastly the class
 * provides any other functions specific for the entries such as printing
 * the entry.
 * @author Tomislav, Kerrod
 *
 */
public class Entries {

	public String classVariable;
	public int size;
	
	public float[] data = new float[8];
	public float[] distance = new float[8];

	public float euclideanDistance;
	
	/**
	 * Entries Constructor takes a list of strings from a line
	 * from a cvs file that has been separated by commas
	 * @param list of strings from a single line in the cvs file
	 */
	public Entries(List<String> list){
		this.size = list.size();
		
		// Initialise all variables
		euclideanDistance = 0;
		for (int i = 0 ; i < list.size() - 1; i++) {
			data[i] = getFloat(list.get(i));
			distance[i] = 0;
		}
		classVariable = list.get(list.size() - 1);
	}
	
	/**
	 * Parses the float value represented in the .csv file
	 * @param s is the String to parse into a float value
	 * @return the float value of s
	 */
	private float getFloat(String s) {
		return Float.parseFloat(s);
	}
	
	/**
	 * Normalise the data
	 * @param i
	 * @param max
	 */
	public void normalise(int i, float max) {
		data[i] /= max;
	}
	
	/**
	 * Gets the ith element of the Entry
	 * @param i is the element to return from the Entry
	 * @return the ith data element in the Entry
	 */
	public float get(int i) {
		if (i < this.size){
			return data[i];
		}
		System.out.println("error");
		return 0;
	}
	
	/**
	 * returns all the elements in the entry as a string format with a
	 * maximum of 4 digits after the decimal point
	 */
	public String toString() {
		String str = "";
		for (int i = 0; i < size - 1; i++) {
			str += String.format("%1.4f", data[i]) + ",\t";
		}
		str += classVariable;
		return str;
	}
	
	/**
	 * returns all the elements in the entry as a string format with a
	 * maximum of 4 digits after the decimal point. This function is
	 * specific for the FileReader class.
	 */
	public String print() {
		String str = "";
		for (int i = 0; i < size - 1; i++) {
			str += Float.toString(data[i]) + ",";
		}
		str += classVariable;
		return str;
	}
	
	/**
	 * @return the euclideanDistance
	 */
	public String getDistances() {
		String str = String.format("%1.4f", euclideanDistance) + "\t";
		return str;
	}
}
