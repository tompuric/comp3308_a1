package com.kwil3092_tpur9893.assignment1.display;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.kwil3092_tpur9893.assignment1.algorithms.Entries;
import com.kwil3092_tpur9893.assignment1.algorithms.FileReader;
import com.kwil3092_tpur9893.assignment1.algorithms.Fold;

/**
 * The DataFrame panel represents information about the data loaded. It provides
 * the application the ability to load and save files for processing as well as
 * displaying the data loaded.
 * @author Tomislav, Kerrod
 *
 */
public class DataFrame extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	private static final int K_FOLDS = 10;
	
	
	private static JTextArea dataTextArea = new JTextArea();
	private static JTextArea informationTextArea = new JTextArea();
	private static JComboBox selection = new JComboBox();
	
	private static FileReader fr = new FileReader();
	public static List<Entries> entries = new ArrayList<Entries>();
	public static Fold[] folds = new Fold[K_FOLDS];
	
	private JScrollPane infoPane = new JScrollPane(informationTextArea);
	private JScrollPane scrollPane = new JScrollPane(dataTextArea);
	
	private int width, height;
	public static Random seed = new Random();
	
	/**
	 * Constructor for DataFrame panel
	 * @param width of panel
	 * @param height of panel
	 */
	public DataFrame(int width, int height) {
		this.width = width;
		this.height = height;
		setPreferredSize(new Dimension(width, height));
		init();
	}
	
	/**
	 * Initialises the DataFrame panel with all its features and user interfaces
	 */
	public void init() {
		scrollPane.setPreferredSize(new Dimension(width - width/10, height - height/10));
		infoPane.setPreferredSize(new Dimension(width - width/10, height/4));
		dataTextArea.setEditable(false);
		informationTextArea.setEditable(false);
		
		for (int i = 0; i < K_FOLDS; i++) {
			selection.addItem("fold" + i);
		}
		
		setLayout(new BorderLayout());
		selection.addActionListener(this);
		add(selection, BorderLayout.NORTH);
		add(scrollPane, BorderLayout.CENTER);
		add(infoPane, BorderLayout.SOUTH);
		
		loadData(FeatureFrame.pimaFile, false);
		
	}
	
	/**
	 * Loads the .cvs file for processing, splits it into K Folds, and displays it
	 * @param filename is the name of the file being read
	 */
	public static void loadData(String filename, boolean randomise) {
		try {
			fr.read(filename);
			if (randomise)
				entries = randomise(fr.trainingData);
			else
				entries = fr.trainingData;
			split(entries, K_FOLDS);
			setDataText(folds[0]);
		} catch (FileNotFoundException e) {
			System.out.println("File was not found: We're looking for '" + filename + "'.");
		} catch (NullPointerException e) {
			System.out.println("File was not found: We're looking for '" + filename + "'.");
		} catch (IOException e) {
			System.out.println("Could not read the file: " + filename);
		}
		
	}
	
	private static List<Entries> randomise(List<Entries> trainingData) {
		List<Entries> entries = new ArrayList<Entries>();
		boolean[] a = new boolean[trainingData.size()];
		
		for (int i = 0; i < trainingData.size(); i++) {
			a[i] = false;
		}
		
		for (int i = 0; i < trainingData.size();) {
			int x = seed.nextInt(trainingData.size());
			if (!a[x]) {
				entries.add(trainingData.get(x));
				i++;
				a[x] = true;
			}
		}

		return entries;
	}

	/**
	 * Splits the list of entries into K_FOLDS of Folds and sets them to the
	 * array accordingly.
	 * @param entries retrieves a list of entries from the loaded file. In
	 * this case it will be 768 entries.
	 * @param K_FOLDS represents how many folds we want to list of entries
	 * to be split into
	 */
	public static void split(List<Entries> entries, int K_FOLDS) {
		List<Entries> class1 = new ArrayList<Entries>();
		List<Entries> class0 = new ArrayList<Entries>();
		class1 = getCase(entries, "1");
		class0 = getCase(entries, "0");
		
		for (int i = 0; i < K_FOLDS; i++) {
			folds[i] = new Fold();
		}
		
		// Entries are added into each fold in a stratified way (even ratio)
		int i =0;
		for (Entries e: class1) {
			if (i >= K_FOLDS) i = 0;
			folds[i].add(e);
			i++;
		}
		i = 0;
		for (Entries e: class0) {
			if (i >= K_FOLDS) i = 0;
			folds[i].add(e);
			i++;
		}
	}
	
	/**
	 * Gets a list of entries depending on what class they belong to
	 * @param entries is the list of entries to retrieve the number of 1/0 classes
	 * @param result is the class to retrieve
	 * @return a list of classes dependent on whether they are case 1 or case 0
	 */
	public static List<Entries> getCase(List<Entries> entries, String result) {
		List<Entries> list = new ArrayList<Entries>();
		for (Entries e: entries) {
			if (e.classVariable.endsWith(result)) list.add(e);
		}
		return list;
	}
	
	/**
	 * @return a string which represents how many class 1 entries and 
	 * class 0 entries there are in the list of entries loaded
	 */
	public static String print() {
		int good = 0, bad = 0;
		for (Entries e : entries) {
			if (e.classVariable.endsWith("1")) good++;
			else bad++;
		}
		return " - " + good + " class 1 entries.\n" + 
		       " - " +  bad + " class 0 entries.\n";
	}
	
	/**
	 * Displays the entries held in each Fold. Only shows the selected fold.
	 * @param fold is the fold to display
	 */
	public static void setDataText(Fold fold) {
		dataTextArea.setText("");
		informationTextArea.setText("");
		for (int i = 0; i < fr.dataTypes.length; i++) {
			dataTextArea.append(fr.dataTypes[i] + ",\t");
		}
		dataTextArea.append("\n");
		for (Entries s : fold.entries) {
			dataTextArea.append(s.toString() + "\n");
		}
		
		informationTextArea.append("In total, we have:\n" +
									" - " + entries.size() + " total entries\n" +
									print());
		informationTextArea.append("In fold " + selection.getSelectedIndex() + " we have:\n" +
									fold.print());
	}

	/**
	 * Resets the data text according to what fold has been selected
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		int i = ((JComboBox) e.getSource()).getSelectedIndex();
		setDataText(folds[i]);
	}

}
