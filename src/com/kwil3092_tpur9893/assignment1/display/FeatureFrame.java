package com.kwil3092_tpur9893.assignment1.display;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.kwil3092_tpur9893.assignment1.algorithms.CrossValidation;

/**
 * FeatureFrame Panel contains all the options for the application. Whether to
 * apply CFS, or change the K value for KNN.
 * @author Tomislav, Kerrod
 *
 */
public class FeatureFrame extends JPanel implements ItemListener, ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String pimaFile = "/pima.csv";
	public static final String pimaCFSFile = "/pima-CFS.csv";
	
	public static CrossValidation cv = new CrossValidation();
	
	@SuppressWarnings("unused")
	private int width, height;
	
	private JLabel kLabel = new JLabel("K value: ");
	public static JComboBox k = new JComboBox();
	private JCheckBox cfsBox = new JCheckBox("Correlation-based Feature Selection");
	private JButton randomiseButton = new JButton("Randomise Data");
	public static String dataToLoad = pimaFile;
	
	/**
	 * Feature Frame Constructor
	 * @param width of panel
	 * @param height of panel
	 */
	public FeatureFrame(int width, int height) {
		this.width = width;
		this.height = height;
		setPreferredSize(new Dimension(width, height/10));
		init();
	}
	
	/**
	 * Initialises the labels, combo box and check box of the application, providing
	 * them their settings and initial values. Also initialises the Cross Validation
	 * class by passing it the data in the form of K Folds.
	 */
	public void init() {
		cfsBox.addItemListener(this);
		randomiseButton.addActionListener(this);
		k.addItem("1");
		k.addItem("5");
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(kLabel);
		add(k);
		add(cfsBox);
		add(randomiseButton);
		cv.init(DataFrame.folds);
	}

	/**
	 * If the check box is deselected, the pima.csv file is loaded
	 * If the check box is selected, the pima-CFS.csv file is loaded
	 * Cross Validation is passed the new Folds and the results are cleared
	 */
	@Override
	public void itemStateChanged(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.DESELECTED) dataToLoad = pimaFile;
		if (e.getStateChange() == ItemEvent.SELECTED) dataToLoad = pimaCFSFile;
		loadData(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		loadData(true);
	}
	
	public static void loadData(boolean randomised) {
		DataFrame.loadData(dataToLoad, randomised);
		cv.init(DataFrame.folds);
		DisplayResults.setKNNText("");
		DisplayResults.setNBText("");
	}


}
