package com.kwil3092_tpur9893.assignment1.display;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * ResultFrame Panel deals with handling the input from the user
 * to apply the KNN and NB algorithms to the data provided and 
 * display their results
 * @author Tomislav, Kerrod
 *
 */
public class ResultFrame extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	private int width, height;
	
	private DisplayResults displayResults;
	private JButton knnButton = new JButton("Calculate KNN");
	private JButton nbButton = new JButton("Calculate NB");
	
	/**
	 * ResultFrame Constructor
	 * @param width of the panel
	 * @param height of the panel
	 */
	public ResultFrame(int width, int height) {
		this.width = width;
		this.height = height;
		setPreferredSize(new Dimension(width, height));
		init();
	}
	
	/**
	 * Initialises the buttons and the results panel to display the results
	 * of the algorithms once the user executes them based on the data listed
	 */
	public void init() {
		displayResults = new DisplayResults(width, height - height/4);
		
		
		knnButton.addActionListener(this);
		nbButton.addActionListener(this);
		knnButton.setPreferredSize(new Dimension(width/2 - 10, height/10));
		nbButton.setPreferredSize(new Dimension(width/2 - 10, height/10));
		
		setLayout(new BorderLayout());
		add(displayResults, BorderLayout.CENTER);
		setLayout(new FlowLayout());
		add(knnButton);
		add(nbButton);
		
	}
	
	/**
	 * Checks which button has been pressed and executed the algorithm which
	 * the user wants.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(knnButton)) {
			FeatureFrame.cv.tickKNN(Integer.parseInt((String) FeatureFrame.k.getSelectedItem()));
		}
		if (e.getSource().equals(nbButton)) {
			FeatureFrame.cv.tickNB();
		}
	}


}
