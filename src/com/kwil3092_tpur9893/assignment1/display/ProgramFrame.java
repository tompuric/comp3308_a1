package com.kwil3092_tpur9893.assignment1.display;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * ProgramFrame controls the right side of the applications graphical user interface
 * @author Tomislav, Kerrod
 *
 */
public class ProgramFrame extends JPanel {

	private static final long serialVersionUID = 1L;
	private int width, height;
	
	public ResultFrame result;
	public FeatureFrame feature;
	private BufferedImage image;
	
	/**
	 * ProgramFrame Constructor
	 * @param width of the panel
	 * @param height of the panel
	 */
	public ProgramFrame(int width, int height) {
		this.width = width;
		this.height = height;
		setPreferredSize(new Dimension(width, height));
		init();
	}
	
	/**
	 * Initialises the right side of the user interface for the application and
	 * adds the results and feature panels to its side. The image is also loaded
	 * and displayed to demonstrate how proud we are of this assignment as we've 
	 * put a lot of effort into it :) WE HAD FUN! AND LEARNT A LOT! =D
	 */
	public void init() {
		result = new ResultFrame(width, height/2);
		feature = new FeatureFrame(width, height/2);
		setLayout(new FlowLayout());
		add(result);
		add(feature);
		
		try {
			// IMAGE WAS TAKEN IN CARSLAW WEST LEARNING HUB ON 06/05/2013 SIGNALING COMPLETION OF CODE
			image = ImageIO.read(this.getClass().getResourceAsStream("/coverAI.png"));
		}
		catch (IOException e) {
			image = new BufferedImage(600, 240, BufferedImage.TYPE_INT_RGB);
		}
	}
	
	/**
	 * Paints the image onto the screen.
	 */
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(image, 40, 375, null);
	}
	
}
