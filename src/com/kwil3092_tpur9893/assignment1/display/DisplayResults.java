package com.kwil3092_tpur9893.assignment1.display;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * DisplayResults Panel displays the results gathered from the KNN and
 * NB classifiers against the given data.
 * @author Tomislav, Kerrod
 *
 */
public class DisplayResults extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int width, height;
	private JLabel title = new JLabel();
	private static JTextArea resultKNN = new JTextArea();
	private JScrollPane scrollPaneKNN = new JScrollPane(resultKNN);
	private static JTextArea resultNB = new JTextArea();
	private JScrollPane scrollPaneNB = new JScrollPane(resultNB);
	
	/**
	 * DisplayResults Default Constructor
	 * @param width of the panel
	 * @param height of the panel
	 */
	public DisplayResults(int width, int height) {
		this.width = width;
		this.height = height;
		setPreferredSize(new Dimension(width, height));
		init();
	}
	
	/**
	 * Initialises and places the text boxes which will display the results of
	 * the KNN and NB algorithms. Text Boxes are empty when first loaded
	 */
	private void init() {
		title.setText("  KNN                                                                                             " +
	                  "  NB");
		
		scrollPaneKNN.setPreferredSize(new Dimension(width/2 - 5, height));
		resultKNN.setEditable(false);		
		
		
		scrollPaneNB.setPreferredSize(new Dimension(width/2 - 5, height));
		resultNB.setEditable(false);
		
		setLayout(new BorderLayout());
		add(title, BorderLayout.NORTH);
		add(scrollPaneKNN, BorderLayout.WEST);
		add(scrollPaneNB, BorderLayout.EAST);
	}
	
	/**
	 * Sets the text for the KNN results text box
	 * @param string to set
	 */
	public static void setKNNText(String string) {
		resultKNN.setText(string);
	}
	
	/**
	 * Appends text to the KNN results text box
	 * @param string to append
	 */
	public static void appendKNNText(String string) {
		resultKNN.append(string);
	}
	
	/**
	 * Sets the text for the NB results text box
	 * @param string to set
	 */
	public static void setNBText(String string) {
		resultNB.setText(string);
	}
	
	/**
	 * Appends text to the NB results text box
	 * @param string to append
	 */
	public static void appendNBText(String string) {
		resultNB.append(string);
	}

}
