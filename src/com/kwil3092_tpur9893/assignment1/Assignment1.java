package com.kwil3092_tpur9893.assignment1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;

import com.kwil3092_tpur9893.assignment1.display.DataFrame;
import com.kwil3092_tpur9893.assignment1.display.ProgramFrame;

/**
 * Splits the frame into two panels. A Data panel representing information
 * about the data loaded which is situation on the left side of the frame
 * and a Program panel representing the purpose and applications of the project
 * on the right side of the frame
 * @author Tomislav, Kerrod
 *
 */
public class Assignment1 extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private int width, height;
	public DataFrame data;
	public ProgramFrame program;

	public Assignment1(int width, int height) {
		this.width = width;
		this.height = height;
		setPreferredSize(new Dimension(width, height));
		setBackground(Color.BLACK);
		init();
	}
	
	public void init() {
		data = new DataFrame(width/2, height);
		program = new ProgramFrame(width/2, height);
		setLayout(new BorderLayout());
		add(data, BorderLayout.WEST);
		add(program, BorderLayout.EAST);
	}

}
