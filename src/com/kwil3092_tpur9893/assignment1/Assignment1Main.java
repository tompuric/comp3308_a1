package com.kwil3092_tpur9893.assignment1;

import java.awt.BorderLayout;

import javax.swing.JFrame;

/**
 * Frame of entire project. Contains all panels and visual interfaces
 * @author Tomislav, Kerrod
 *
 */
public class Assignment1Main extends JFrame {

	public static final int WIDTH = 1200;
	public static final int HEIGHT = 640;
	private static final long serialVersionUID = 1L;
	private static final String title = "COMP3308 Assignment 1 by Kerrod Williams and Tomislav Puric";
	private Assignment1 a1;
	
	public Assignment1Main(String title) {
		super(title);
		a1 = new Assignment1(WIDTH, HEIGHT);
		
		setSize(WIDTH, HEIGHT);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
		
		
		setLayout(new BorderLayout());
		add(a1, BorderLayout.CENTER);
		
		pack();
	}

	/**
	 * Main method of program. Executes application and runs the Frame
	 * @param args
	 */
	public static void main(String[] args) {
		new Assignment1Main(title);
	}

}
